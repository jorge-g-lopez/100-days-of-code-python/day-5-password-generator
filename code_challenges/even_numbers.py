""" Adding even numbers """

total = 0

# Add even numbers from 1 to 100

for number in range(2, 101, 2):
    total += number

print(total)
