""" Average Height """

student_heights = input("Input a list of student heights ").split()
for n in range(0, len(student_heights)):
    student_heights[n] = int(student_heights[n])
print(student_heights)

# Not using len nor sum

sum_heights = 0
count_heights = 0
for n in student_heights:
    sum_heights += n
    count_heights += 1

average_height = round(sum_heights / count_heights)
print(average_height)
